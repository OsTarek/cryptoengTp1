#include<stdio.h>
#include<string.h>
#include<aes-128_enc.h>

void printBuffer(uint8_t * buffer, int len) {
    for (int i = 0; i < len; i++) {
        printf("%02X", buffer[i]);
    }
    putchar('\n');
}

int main() {
    uint8_t toEncrypt[AES_BLOCK_SIZE];
    uint8_t encrypted[AES_BLOCK_SIZE];
    uint8_t key[AES_128_KEY_SIZE] = "0123456789ABCDEF";
    unsigned rounds = 4;
    int lastfull = 0;
    strcpy(toEncrypt, "Some Plain Text");
    memcpy(encrypted, toEncrypt, AES_BLOCK_SIZE);

    aes128_enc(encrypted, key, rounds, lastfull);

    printf("\nPlain text:\n%.16s\n\nBlock content:\n", toEncrypt);
    printBuffer(toEncrypt, AES_BLOCK_SIZE);
    printf("\nCypher text block content:\n");
    printBuffer(encrypted, AES_BLOCK_SIZE);
    putchar('\n');
    return 0;
}